package com.kostiantynlelikov;

public class Circle {
    private double x;
    private double y;
    private double radius;
    private static final double PI = 3.14;

    public Circle(double x, double y, double radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    public double S() {
        return Math.pow(radius, 2) * PI;
    }
}
